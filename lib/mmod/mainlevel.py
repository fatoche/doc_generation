"""
Package comment main level.
"""


class MainClass(object):
    """
    Class comment main level.
    """

    def __init__(self, value):
        """
        Init comment main level.

        :param int value: some value that will be printed
        """
        self.a = 1
        print(value)

    def get_value(self):
        """
        Return a.
        
        :return: a
        :rtype: int
        """
        return self.a

    @staticmethod
    def print_level():
        print('main')
