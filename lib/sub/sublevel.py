"""
Package comment sub level.
"""


class SubClass(object):
    """
    Class comment sub level.
    """

    def __init__(self):
        """
        Init comment sub level.
        
        :param int value: some value that will be printed
        """
        self.a = 3

    def get_value(self):
        """
        Return a.

        :return: a
        :rtype: int
        """
        return self.a

    def add(self, some_number):
        """
        Add a number to a.

        :param int some_number: the number
        :return: sum of a and the number
        """
        return self.a + some_number

    @staticmethod
    def print_level():
        """
        Print function

        :return: None
        """
        print('sub')
