"""
Package comment main level.
"""

from typing import List


class MainClass(object):
    """
    Class comment main level.
    """

    def __init__(self, value):
        """
        Init comment main level.

        :param int value: some value that will be printed
        """
        self.a = 1
        print(value)

    def get_value(self):
        """
        Return a.
        
        :return: a
        :rtype: int
        """
        return self.a
    
    def add_list(self, some_list: List[int]) -> List[int]:
        """
        Add a to each element of the list.
        
        :param some_list: a list.
        :return: Return the list with added a.
        """
        return [element + self.a for element in some_list]
    
    def add_all(self, b: int, c: int, d: int, **kwargs) -> int:
        """
        Add all parameters to a and return the sum.
        
        :param b: first parameter
        :param c: second parameter
        :param d: third parameter
        :param kwargs: additional parameters
        :return: the sum
        """
        return self.a + b + c + d + sum(list(kwargs.values()))
        

    @staticmethod
    def print_level():
        print('main')
