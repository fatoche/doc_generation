"""
Package comment lib level.
"""


class LibClass(object):
    """
    Class comment lib level.
    """

    def __init__(self):
        """
        Init comment lib level.
        
        :param int value: some value that will be printed
        """
        self.a = 2

    def get_value(self):
        """
        Return a.
        
        :return: a
        :rtype: int
        """
        return self.a
    
    def add(self, some_number: int):
        """
        Add a number to a.
        
        :param int some_number: the number 
        :return: sum of a and the number
        """
        return self.a + some_number

    @staticmethod
    def print_level():
        """
        Print function
        
        :return: None
        """
        print('lib')
