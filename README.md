## Helpful links

- [docstring syntax](https://www.jetbrains.com/help/pycharm/using-docstrings-to-specify-types.html)
- [generate Sphinx documentation with PyCharm](https://www.jetbrains.com/help/pycharm/generating-reference-documentation.html)
- [Sphinx documentation](http://www.sphinx-doc.org/en/master/usage/restructuredtext/domains.html)
- [simple tutorial](https://samnicholls.net/2016/06/15/how-to-sphinx-readthedocs/)

## Approach

- install sphinx in your virtualenv:

    `pip install sphinx`
- create a subdirectory `docs`:
    ```bash
    mkdir docs
    cd docs
    ```
- initialize Sphinx:

    `sphinx-quickstart`
- add autodoc (the tool that extract information from doc strings) to extensions in `docs/conf.py`:
    ```python
    extensions = [
        'sphinx.ext.autodoc'
    ]
    ```
- init autodoc, apidoc creates stubs which can tell autodoc what to do
    ```bash
    cd docs/
    sphinx-apidoc -o source/ ../lib
    ```
- run sphinx (including autodoc)
    ```
    make html
    ```
    
- after changes inside existing files rerun `make html`
- after adding/moving/deleting directories or files run (remember \_\_init\_\_.py in new directories!):
    ```bash
    sphinx-apidoc -f -o source/ ../lib
    make html
    ```
    
## How to write docstrings

- one-line: `""" Short description. """`
- multi-line:
    ```python
    """
    Short description, one line.
  
    Longer description, one or multiple lines.
  
    :param PARAMETER_TYPE PARAMETER_NAME: parameter description
    ...
    :return: Description of the return value.
    :rtype: Type of the return value.
    """
    ```
- empty line between description and the parameter list is necessary for correct rendering
- type annotations are recognized by Sphinx and can be used instead of the type description in the docstring. In that
 case, leave out PARAMETER_TYPE.
